import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { useNavigate } from "react-router-dom";
import axios from "axios";
 

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  container: {
    marginTop: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
}));

export default function Inf() {
  const classes = useStyles();

   

  const URLVotes = process.env.REACT_APP_URL + "/inf/";
  const [votes, setVotes] = useState([]);
  useEffect(() => {
    getVoters();
  }, []);
  
  const getVoters = async () => {

    const token = localStorage.getItem("token");
    try {
      const response = await axios.get(`${URLVotes}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // console.log(response.data);
      setVotes(response.data);
      // console.log(response.data);
    } catch (err) {
      // console.log('er',err);
    }
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/admin");
    }
  });
 
    
   
 
  // className={classes.container  }
  return (
    <div className="main-ned">
      <div>
   
      </div>
      <div>
        <Container className="needer_container" maxWidth="lg">
          <Paper className="paper">
            {/* className={classes.paper} */}
            <Box display="flex">
              <Box flexGrow={1}>
                <Typography
                  component="h2"
                  variant="h4"
                  color="primary"
                  gutterBottom
                  style={{ padding: "1.5rem" }}
                >
                  Votes 
                </Typography>
              </Box>
            </Box>
            <TableContainer>
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow className="list-1">
                    <TableCell align="center">INF_ID</TableCell>
                    <TableCell align="center">Influencer</TableCell>
                    <TableCell align="center">Gender</TableCell>
                    <TableCell align="center">Type</TableCell>
                    <TableCell align="center">Governorate</TableCell>
                    <TableCell align="center">City</TableCell>
                   
                    {/* <TableCell align="center">Status</TableCell> */}
             
                  </TableRow>
                  
                </TableHead>    
                <TableBody> 
                  {votes.map((vote) => (
                    
                    <TableRow key={vote._id} className="list-2">
                      <TableCell align="center">{vote._id}</TableCell>  
                    
                      <TableCell align="center">{vote.name}</TableCell>
                      <TableCell align="center">{vote.voters.name}</TableCell>  
                      <TableCell align="center">{vote.id_inf}</TableCell>
                      <TableCell align="center">{vote.voters.name}</TableCell>   
                      <TableCell align="center">{vote.bloodGroup}</TableCell>
                      
                      {/* <TableCell align="left">{donor.Donor}</TableCell> */}

                      <TableCell align="center">
                        <ButtonGroup
                          color="primary"
                          aria-label="outlined primary button group"
                        >
                          {/* {user ? (
                            <Button >
                              Edit
                            </Button>
                          ) : (
                            ""
                          )} */}
                          {/* <Button onClick={() => UserDelete(vote._id)}>
                            Del
                          </Button> */}
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </div>
    </div>
  );
}
