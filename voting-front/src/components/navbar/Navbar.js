// import React from "react";
// import "./navbar.css";
// import { Navigate, useNavigate } from "react-router-dom";

// function Navbar() {
//   const navigate = useNavigate();
//   const handleClick = () => {
//     localStorage.removeItem("token");
//     navigate("/login");
//   };

//   return (
//     <>
// <ul className="ul">
//   <li className="li">
//           <button className="active" onClick={handleClick}>
//             Logout
//           </button>
//   </li>
// </ul>
//     </>
//   );
// }

// export default Navbar;

import * as React from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import { Navigate, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios";
import "./navbar.css";

export default function BasicMenu() {
  const URLMe = process.env.REACT_APP_URL + "/users/me";
  const [mes, setMes] = useState([]);
  useEffect(() => {
    getMe();
  }, []);

  const getMe = async () => {
    const token = localStorage.getItem("token");
    try {
      const response = await axios.get(`${URLMe}`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      // console.log(response.data);
      setMes(response.data);
      // console.log(response.data);
    } catch (err) {
      // console.log('er',err);
    }
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const navigate = useNavigate();
  const Logout = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <div className="nav-con">
      <ul className="ul">
        <li className="li">
          <Button 
            id="basic-button"
            aria-controls={open ? "basic-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
            // className="active"
            onClick={handleClick}
          >
            {mes.name}
          </Button>
          <Menu
            id="basic-menu"
            anchorEl={anchorEl}
            open={open}
            onClose={handleClose}
            background="red"
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem className="active" onClick={Logout}>
              Logout
            </MenuItem>
          </Menu>
        </li>
      </ul>
    </div>
  );
}
