import React from "react";
import "../../pages/home.css";
import kingco from "../../Assets/kingco.png";
import khabar from "../../Assets/khabar.png";
import anbar from "../../Assets/anbar.png";
import gi from "../../Assets/gi.png";
function Logos() {
  return (
    <div className="logos">
      <a
        href="https://www.instagram.com/golden.international2022/?igshid=YmMyMTA2M2Y%3D"
        style={{ target: "blank" }}
      >
        <img src={gi} />
      </a>
      <a
        href="https://www.instagram.com/kingco.socialmedia/?hl=en"
        style={{ target: "blank" }}
      >
        <img src={kingco} alt="Kingcomedia" />
      </a>
      <a href="https://www.khabar3ajel.com/" style={{ target: "blank" }}>
        <img src={khabar} alt="KhabarAjel" />
      </a>
      <a
        href="https://www.instagram.com/3anbar_restaurant/?hl=en"
        style={{ target: "blank" }}
      >
        <img src={anbar} alt="3anbar_restaurant" />
      </a>
    </div>
  );
}

export default Logos;
